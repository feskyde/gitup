# GitUp Changelog

### GitUp v0.1 "Castanets" Release

#### Changes:

- **Bolded** input messages and script output.
- Usage of functions.
  - The functions are stored on a gitup.funcs file.
- Rewrite the original GitUp commands:
  - `guconf`
  - `luconf`
  - `init`
  - `rsetup`
  - `commit`
  - `status`
  - and the branch management commands

### GitUp v0.1 "Castanets" Bugfix 1

#### Fixes:

- Removed `functions` folder (makes INSTALLER and gitup script go crazy).
- Added colors to `gitup` script.

### GitUp v0.2 "Drums" Release

#### Changes:

- Added `rfetch`, for fetching pull requests.
- More pull request utilities will be added on the future.
- Change the usage of functions to simple parenthesis.
- Moved `help` and `about` sections to functions file.

### GitUp v0.3 "Euphonium" Release

#### Changes:

- A rewrite, minimizing the LOC of original script.
- Added a lot of new options:
  - `fetch`
  - `garbage-collector`
  - `check-filesystem`
  - `reset`
  - `log`
  - `ls-tree`
  - `diff`
  - `grep`
  - `merge`
- Change of license (from WTFPL to 3-Clause BSD).
- A new brand installer (`Universal Script Installer`).

### GitUp v1.0 "Adagio" Release

#### Changes:

- Minimized LOC (from +360 to -340).
- Some functions are rebranded.
- The script is now more legible.
- Fixed the bug that makes the `about` section dependant of `\n` characters.
- The `USI` is updated to DRB (Daily Build Releases) "Allegro"
- Make GitUp available for other git-based solutions than GitHub.
- Optimizations.
- Some functions of GitUp 0.3-1 are rewritten.

### GitUp v1.0 "Adagio" Bugfix 1

#### Fixes:

- Debian shell coloration problem (you need to install using `INSTALLER`)

### GitUp v1.0 "Adagio" Bugfix 2

#### Fixes:

- Back to `echo` + `read` input metod.
- Fixes under traductions.

### GitUp v2.0 "Ballet" Release

#### Changes:

- Removed unnecesary LOC (300 to 230)
- Added ASCII art
- `print-gitweb` is now part of `about`
- `version` is now linked to `about`
- `fetch-request` is removed
- Some commands are rebranded:
  - `garbage-collector` is now `collector`
  - `check-filesystem` is now `fscheck`
  - `repo-setup` is now `setup`
  - `ls-tree` is now `tree`
  - `branch-[action]` is now `b[action]`
- `local-config` and `global-config` are united (use `config`)
- USI gets updated to 2015.10.31
- LICENSE gets updated

